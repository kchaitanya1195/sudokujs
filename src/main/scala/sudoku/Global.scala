package sudoku

import sudoku.models.PuzzleM.CellData

import scala.scalajs.js

object Global {
  def getColumn(ind: Int): Int = (ind % 9) + 1
  def getRow(ind: Int): Int = (ind / 9) + 1
  def getRowColumn(ind: Int): (Int, Int) = (getRow(ind), getColumn(ind))
  def getInd(r: Int, c: Int): Int = r * 9 + c
  implicit class RichJS(val obj: js.Object) extends AnyVal {
    def merge(other: js.Object): js.Object = {
      val result = js.Dictionary.empty[Any]
      for ((key, value) <- obj.asInstanceOf[js.Dictionary[Any]])
        result(key) = value
      for ((key, value) <- other.asInstanceOf[js.Dictionary[Any]])
        result(key) = value

      result.asInstanceOf[js.Object]
    }
  }

  val indexes: Seq[Int] = 0 to 80
  // List of all possible rows, cols and boxes
  val unitList: Seq[Seq[Int]] = {
    val rows = indexes.grouped(9).toSeq
    val cols = rows.transpose
    val grids = for {
      rowGrid <- Seq(0 to 2, 3 to 5, 6 to 8)
      colGrid <- Seq(0 to 2, 3 to 5, 6 to 8)
    } yield {
      for {
        r <- rowGrid
        c <- colGrid
      } yield 9 * r + c
    }

    rows ++ cols ++ grids
  }
  // Map of a square to list of units that contain that square
  val unitMap: Map[Int, Seq[Seq[Int]]] = indexes.map(i =>
    i -> unitList.filter(_ contains i)
  ).toMap
  // Map of square to list of squares that are in the same unit
  val peerMap: Map[Int, Seq[Int]] = unitMap.map { case (i, units) =>
    i -> units.flatten.filter(_ != i)
  }

  val singleSets: Map[Int, Set[Int]] = (1 to 9).map(i => i -> Set(i)).toMap
  val filledSet: Seq[Int] = 1 to 9

  val selectColor: String = "#cd5f57aa"
  val highlightColor: String = "#32a0a844"
  val disabledColor: String = "#9e9e9eaa"
  val highlightLightColor: String = "#32a0a8aa"
  val questionColor: String = "#326fa8aa"

  def getPeerCells(index: Int, cellMap: Map[Int, CellData], solved: Boolean): Seq[CellData] =
    peerMap(index)
      .map(cellMap)
      .filter(_.solved == solved)
}
