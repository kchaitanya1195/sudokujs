package sudoku

import scala.scalajs.js
import scala.scalajs.js.annotation.JSExportTopLevel

object App {

//  def main(args: Array[String]): Unit = {
//
//    ReactDOM.render(
//      App(puzzle),
//      document.getElementById("root")
//    )
//  }

  @JSExportTopLevel("app")
  val app: js.Object = Sudoku.componentConstructor
}