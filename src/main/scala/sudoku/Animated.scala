package sudoku

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.|

@js.native
@JSImport("react-native", "Animated")
class Animated extends js.Object {
  def start(): Unit = js.native
}

@js.native
@JSImport("react-native", "Animated")
object Animated extends js.Object {
  @js.native
  class Value(value: Int) extends js.Object {
    def interpolate(config: js.Object): js.Any = js.native
  }

  def timing(value: Value, config: js.Object): Animated = js.native
  def sequence(animations: js.Array[Animated]): Animated = js.native
}

@react object AnimatedView extends ExternalComponent {
  case class Props(style: js.UndefOr[js.Object] = js.undefined)

  @js.native
  @JSImport("react-native", "Animated.View")
  object View extends js.Object

  override val component: String | js.Object = View
}
