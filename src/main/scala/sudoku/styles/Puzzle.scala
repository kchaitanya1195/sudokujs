package sudoku.styles

import sudoku.{Animated, Global}
import sudoku.Global.RichJS

import scala.scalajs.js
import scala.scalajs.js.Dynamic.literal

object Puzzle {
  val fontFamily = "Roboto"

  val grid: js.Object = literal(
    display = "flex",
    alignSelf = "center",
    borderWidth = 2,
    borderColor = "black",
    width = "90%",
    aspectRatio = 1,
    margin = "5%",
    marginTop = "10%"
  )

  val row: js.Object = literal(
    display = "flex",
    flexDirection = "row",
    justifyContent = "center",
    flex = 1
  )
  val rowTopBorder: js.Object = row.merge(literal(
    borderTopWidth = 1,
    borderColor = "black"
  ))
  val rowBottomBorder: js.Object = row.merge(literal(
    borderBottomWidth = 2,
    borderColor = "black"
  ))

  val selectedNum: js.Object = literal(
    backgroundColor = Global.selectColor
  )

  val rowStyles: Map[Int, js.Object] = (0 until 9).map { r =>
    val rowStyle = r match {
      case 0 => Puzzle.rowTopBorder
      case x if x % 3 == 2 => Puzzle.rowBottomBorder
      case _ => Puzzle.row
    }

    r -> rowStyle
  }.toMap

  def flashErrorBackground(errorBackground: Animated.Value): Unit = {
    Animated.sequence(js.Array(
      Animated.timing(
        errorBackground,
        literal(
          toValue = 1,
          duration = 200,
          useNativeDriver = false
        )
      ),
      Animated.timing(
        errorBackground,
        literal(
          toValue = 0,
          duration = 500,
          useNativeDriver = false
        )
      )
    )).start()
  }
}
