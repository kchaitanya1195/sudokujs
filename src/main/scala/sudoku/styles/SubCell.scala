package sudoku.styles

import sudoku.Global
import sudoku.Global.RichJS

import scala.scalajs.js
import scala.scalajs.js.Dynamic.literal

object SubCell {
  private val subCellTextSize = 7

  private val subCell: js.Object = literal(
    display = "flex",
    flex = 1,
    aspectRatio = 1,
    justifyContent = "center"
  )
  val subCellText: js.Object = literal(
    textAlign = "center",
    fontFamily = Puzzle.fontFamily,
    lineHeight = subCellTextSize,
    fontSize = subCellTextSize
  )

  private val onlyUnitPossibility: js.Object = subCell.merge(literal(
    backgroundColor = Global.highlightLightColor
  ))
  private val selectedNum: js.Object =
    subCell.merge(Puzzle.selectedNum)

  def subCellStyle(isLastInUnit: Boolean, isHighlighted: Boolean): js.Object = {
    if (isLastInUnit)
      onlyUnitPossibility
    else if (isHighlighted)
      selectedNum
    else
      subCell
  }
}
