package sudoku.styles

import sudoku.{Animated, Global}
import sudoku.Global.RichJS
import sudoku.models.PuzzleM.PuzzleMode

import scala.scalajs.js
import scala.scalajs.js.Dynamic.literal

object Cell {
  private val cellTextSize = 20

  private val cell: js.Object = literal(
    display = "flex",
    borderWidth = 1,
    borderColor = "black",
    flex = 1,
    justifyContent = "center"
  )
  val cellText: js.Object = literal(
    textAlign = "center",
    fontFamily = Puzzle.fontFamily,
    lineHeight = cellTextSize,
    fontSize = cellTextSize
  )
  val questionCellText: js.Object = cellText.merge(literal(
    color = Global.questionColor
  ))
  private val cellLeftBorder: js.Object = cell.merge(literal(
    borderLeftWidth = 2,
    borderColor = "black"
  ))
  private val cellRightBorder: js.Object = cell.merge(literal(
    borderRightWidth = 2,
    borderColor = "black"
  ))
  private val singlePossibilityCell: js.Object = literal(
    backgroundColor = Global.highlightColor
  )
  val subRow: js.Object = literal(
    display = "flex",
    flexDirection = "row",
    justifyContent = "center"
  )

  private val cellStyles: Map[Int, js.Object] = (0 until 9).map { c =>
    val cellStyle = c match {
      case 0 => cellLeftBorder
      case x if x % 3 == 2 => cellRightBorder
      case _ => cell
    }

    c -> cellStyle
  }.toMap
  private val singlePossibilityCellStyles: Map[Int, js.Object] =
    cellStyles.view.mapValues(_.merge(singlePossibilityCell)).toMap
  private val selectedCellStyles: Map[Int, js.Object] =
    cellStyles.view.mapValues(_.merge(Puzzle.selectedNum)).toMap

  def unsolvedCellStyle(col: Int, hasSinglePossibility: Boolean,
                        puzzleMode: PuzzleMode.PuzzleMode, flashAnim: Animated.Value): js.Object = {
    (if (hasSinglePossibility && puzzleMode == PuzzleMode.Question)
      singlePossibilityCellStyles(col)
    else
      cellStyles(col))
      .merge(literal(
        backgroundColor = flashAnim.interpolate(literal(
          inputRange = js.Array(0, 1),
          outputRange = js.Array("rgba(255, 0, 0, 0)", "rgba(255, 0, 0, 0.6)")
        ))
      ))
  }
  def solvedCellStyle(col: Int, isSelected: Boolean): js.Object = {
    if (isSelected)
      selectedCellStyles(col)
    else
      cellStyles(col)
  }
}
