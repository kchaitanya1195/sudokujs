package sudoku.styles

import sudoku.Global
import sudoku.Global.RichJS
import sudoku.models.PadM.KeyState

import scalajs.js
import scalajs.js.Dynamic.literal

object KeyPad {
  val numPad: js.Object = literal (
    display = "flex",
    flexDirection = "row",
    alignSelf = "center",
    borderWidth = 2,
    borderColor = "black",
    width = "90%",
    margin = "5%"
  )
  val numKey: js.Object = literal(
    display = "flex",
    borderWidth = 1,
    borderColor = "black",
    flex = 1,
    aspectRatio = 1,
    justifyContent = "center"
  )
  private val keySelected: js.Object = numKey.merge(literal(
    backgroundColor = Global.selectColor
  ))
  private val keyDisabled: js.Object = numKey.merge(literal(
    backgroundColor = Global.disabledColor
  ))

  def numKeyStyle(keyState: KeyState.KeyState): js.Object = {
    keyState match {
      case KeyState.Default =>
        numKey
      case KeyState.Selected =>
        keySelected
      case KeyState.Disabled =>
        keyDisabled
    }
  }
}
