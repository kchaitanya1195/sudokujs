package sudoku.models

import sudoku.Animated

object PuzzleM {
  object CellState extends Enumeration {
    type CellState = Value
    val Solved, Unsolved, Fixed = Value
  }
  object PuzzleMode extends Enumeration {
    type PuzzleMode = Value
    val Question, Note, Input = Value
  }
  object SwitchEvent extends Enumeration {
    type SwitchEvent = Value
    val New, Solve, Reset, Note = Value
  }

  type CellIndex = Int
  type Possibility = Int

  case class CellData(index: CellIndex,
                      state: CellState.CellState,
                      possibilities: Set[Possibility],
                      cellValue: Int,
                      flashAnim: Animated.Value) {
    val solved: Boolean = state != CellState.Unsolved
  }
}
