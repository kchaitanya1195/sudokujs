package sudoku.models

object PadM {
  object KeyState extends Enumeration {
    type KeyState = Value
    val Default, Selected, Disabled = Value
  }
}
