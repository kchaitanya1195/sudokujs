package sudoku.components

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.|

@react object Icon extends ExternalComponent {
  case class Props(name: String, size: Int, solid: Boolean = false, style: js.UndefOr[js.Object] = js.undefined)

  @js.native
  @JSImport("react-native-vector-icons/MaterialCommunityIcons", JSImport.Default)
  object Icon extends js.Object

  override val component: String | js.Object = Icon
}
