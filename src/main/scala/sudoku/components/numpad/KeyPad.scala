package sudoku.components.numpad

import slinky.core.StatelessComponent
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.native._
import sudoku.components.TouchableWithoutFeedback
import sudoku.models.PadM.KeyState
import sudoku.models.PuzzleM.{PuzzleMode, SwitchEvent}
import sudoku.styles.KeyPad._
import sudoku.styles.Cell

@react class KeyPad extends StatelessComponent {
  case class Props(selected: Option[Int], disabledKeys: Set[Int], mode: PuzzleMode.PuzzleMode,
                   onKeySelected: Int => Unit, switchMode: SwitchEvent.SwitchEvent => Unit)

  def switchToSolveMode(): Unit = {
    if (props.mode == PuzzleMode.Question)
      props.switchMode(SwitchEvent.Solve)
    else
      props.switchMode(SwitchEvent.Reset)
  }

  def render(): ReactElement = {
    View(style = numPad)(
      (1 to 9).map { i =>
        val keyState =
          if (props.disabledKeys.contains(i))
            KeyState.Disabled
          else if (props.selected.contains(i))
            KeyState.Selected
          else KeyState.Default
        NumKey(i,keyState, props.onKeySelected)
          .withKey(i.toString)
      },
      NoteKey(props.mode, () => props.switchMode(SwitchEvent.Note)).withKey("N"),
      TouchableWithoutFeedback(onPress = () => props.switchMode(SwitchEvent.New))(
        View(style = numKey)(Text(style = Cell.cellText, selectable = false)("New"))
      ),
      TouchableWithoutFeedback(onPress = switchToSolveMode _)(
        View(style = numKey)(Text(style = Cell.cellText, selectable = false)(
          if (props.mode == PuzzleMode.Question) "Solve" else "Reset"
        ))
      )
    )
  }
}
