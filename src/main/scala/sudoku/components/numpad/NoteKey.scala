package sudoku.components.numpad

import slinky.core.StatelessComponent
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.native.View
import sudoku.components.TouchableWithoutFeedback
import sudoku.styles.{Cell, KeyPad}
import sudoku.components.Icon
import sudoku.models.PuzzleM.PuzzleMode

@react class NoteKey extends StatelessComponent {
  case class Props(mode: PuzzleMode.PuzzleMode, switchNoteMode: () => Unit)

  def render(): ReactElement = {
    TouchableWithoutFeedback(onPress = () => props.switchNoteMode())(
      View(style = KeyPad.numKey)(
        if (props.mode == PuzzleMode.Note) Icon(style = Cell.cellText, name = "pencil", size = 30)
        else Icon(style = Cell.cellText, name = "pencil-outline", size = 30)
      )
    )
  }
}
