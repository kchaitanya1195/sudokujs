package sudoku.components.numpad

import slinky.core.StatelessComponent
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.native.{Text, View}
import sudoku.components.TouchableWithoutFeedback
import sudoku.models.PadM.KeyState
import sudoku.styles.KeyPad.numKeyStyle
import sudoku.styles.Cell

@react class NumKey extends StatelessComponent {
  case class Props(keyValue: Int,
                   keyState: KeyState.KeyState,
                   onSelected: Int => Unit)

  override def shouldComponentUpdate(nextProps: Props, nextState: Unit): Boolean = {
    props.keyState != nextProps.keyState
  }

  def render(): ReactElement = {
    val keyStyle = numKeyStyle(props.keyState)

    TouchableWithoutFeedback(onPress = () => if (props.keyState != KeyState.Disabled) props.onSelected(props.keyValue))(
      View(style = keyStyle)(Text(style = Cell.cellText, selectable = false)(props.keyValue))
    )
  }
}
