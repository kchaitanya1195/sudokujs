package sudoku.components

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.|

@react object TouchableWithoutFeedback extends ExternalComponent {
  case class Props(onPress: js.UndefOr[() => Unit] = js.undefined, style: js.UndefOr[js.Object] = js.undefined)

  @js.native
  @JSImport("react-native", "TouchableWithoutFeedback")
  object Component extends js.Object

  override val component: String | js.Object = Component
}
