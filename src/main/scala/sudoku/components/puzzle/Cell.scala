package sudoku.components.puzzle

import slinky.core.StatelessComponent
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.native.{Text, View}
import slinky.web.html.{key, style}
import sudoku.AnimatedView
import sudoku.Global.getInd
import sudoku.components.TouchableWithoutFeedback
import sudoku.models.PuzzleM.{CellData, CellState, Possibility, PuzzleMode}
import sudoku.styles.Cell._

@react class Cell extends StatelessComponent {
  case class Props(row: Int, col: Int, data: CellData,
                   lastPossibilities: Option[Possibility],
                   hasSinglePossibility: Boolean, mode: PuzzleMode.PuzzleMode,
                   onCellClicked: Int => Unit, selectedKey: Option[Int])

  def render(): ReactElement = {
    val index = getInd(props.row, props.col)

    if (!props.data.solved) {
      // Highlight cell if it has only one possibility
      val cellStyle =
        unsolvedCellStyle(props.col, props.hasSinglePossibility, props.mode, props.data.flashAnim)
      TouchableWithoutFeedback(onPress = () => props.onCellClicked(index))(
        AnimatedView(style = cellStyle)(
          (0 until 3).map(r =>
            View(style := subRow, key := r.toString)(
              (0 until 3).map { c =>
                // Display note in the index that matches it (eg: 3 in top right cell)
                val note = (3 * r + c) + 1
                val isSubCellHighlighted = props.selectedKey.contains(note)
                if (props.data.possibilities.contains(note))
                  SubCell(note, isSubCellHighlighted, props.lastPossibilities.contains(note))
                    .withKey(note.toString)
                else
                  SubCell(0, isHighlighted = false, isLastInUnit = false).withKey(note.toString)
              }
            )
          )
        )
      )
    } else {
      val cellStyle = solvedCellStyle(props.col, props.selectedKey.contains(props.data.cellValue))
      val textStyle =
        if (props.data.state == CellState.Fixed)
          questionCellText
        else cellText
      TouchableWithoutFeedback(onPress = () => props.onCellClicked(index))(
        View(style = cellStyle)(Text(style = textStyle, selectable = false)(props.data.cellValue))
      )
    }
  }
}
