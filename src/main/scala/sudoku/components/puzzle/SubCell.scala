package sudoku.components.puzzle

import slinky.core.StatelessComponent
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.native.{Text, View}
import sudoku.styles.SubCell._

@react class SubCell extends StatelessComponent {
  case class Props(value: Int, isHighlighted: Boolean, isLastInUnit: Boolean)

  override def render(): ReactElement = {
    val cellStyle = subCellStyle(props.isLastInUnit, props.isHighlighted)
    val text = if (props.value > 0) props.value.toString else ""
    View(style = cellStyle)(
      Text(style = subCellText, selectable = false)(text)
    )
  }
}
