package sudoku.components.puzzle

import slinky.core.StatelessComponent
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.native.View
import slinky.web.html.{key, style}
import sudoku.Global._
import sudoku.models.PuzzleM._
import sudoku.styles.Puzzle

@react class Grid extends StatelessComponent {
  case class Props(cells: Map[Int, CellData], lastPossibilities: Map[CellIndex, Possibility],
                   selectedKey: Option[Int], mode: PuzzleMode.PuzzleMode, onCellClicked: Int => Unit)

//  override def initialState: State = {
////    val cells = (0 until 81).map { i =>
////      if (props.puzzle(i) == -1) {
////        val peerValues = peerMap(i).map(props.puzzle).filter(_ != -1)
////        val notes = filledSet diff peerValues
////
////        i -> CellData(i, CellState.Unsolved, notes.toSet, 0)
////      } else {
////        i -> CellData(i, CellState.Fixed, Set.empty, props.puzzle(i))
////      }
////    }.toMap
////
////    val lastPossibilities = getLastPossibilities(cells)
//    val cells = (0 to 80).map(i => i -> CellData(i, CellState.Unsolved, Set.empty, 0)).toMap
//    State(Map.empty, cells)
//  }

  def render(): ReactElement = {
    View(style = Puzzle.grid)(
      (0 until 9).map { r =>
        View(style := Puzzle.rowStyles(r), key := r.toString)(
          (0 until 9).map { c =>
            val index = getInd(r, c)
            val cellData = props.cells(index)
            val hasSinglePossibility =
              (cellData.possibilities.size == 1) &&
                !getPeerCells(index, props.cells, solved = true)
                  .map(_.cellValue)
                  .distinct.contains(cellData.possibilities.head)

            Cell(r, c, cellData, props.lastPossibilities.get(index), hasSinglePossibility,
              props.mode, props.onCellClicked, props.selectedKey)
              .withKey(index.toString)
          }
        )
      }
    )
  }
}
