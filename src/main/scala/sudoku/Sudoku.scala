package sudoku

import slinky.core.Component
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.native.View
import sudoku.Global.{filledSet, getPeerCells, peerMap, unitList}
import sudoku.components.puzzle.Grid
import sudoku.components.numpad.KeyPad
import sudoku.models.PuzzleM.{CellData, CellIndex, CellState, Possibility, PuzzleMode, SwitchEvent}
import sudoku.styles.Puzzle.flashErrorBackground

import scala.scalajs.js.Dynamic.literal

@react class Sudoku extends Component {
  type Props = Unit
  case class State(cells: Map[Int, CellData], lastPossibilities: Map[CellIndex, Possibility],
                   selectedKey: Option[Int], disabledKeys: Set[Int], mode: PuzzleMode.PuzzleMode)

  override def initialState: State = {
    val cells = (0 to 80)
      .map(i => i -> CellData(i, CellState.Unsolved, Set.empty, 0, new Animated.Value(0)))
      .toMap
    State(cells, Map.empty, None, Set.empty, PuzzleMode.Question)
  }

  def onKeySelected(key: Int): Unit = {
    if (state.selectedKey.contains(key))
      setState(state.copy(selectedKey = None))
    else
      setState(state.copy(selectedKey = Some(key)))
  }

  def switchMode(newMode: SwitchEvent.SwitchEvent): Unit = {
    newMode match {
      case SwitchEvent.New =>
        val cells = (0 to 80)
          .map(i => i -> CellData(i, CellState.Unsolved, Set.empty, 0, new Animated.Value(0)))
          .toMap
        setState(State(cells, Map.empty, None, Set.empty, PuzzleMode.Question))
      case SwitchEvent.Solve | SwitchEvent.Reset =>
        val cells = state.cells.view.mapValues { cellData =>
          if (cellData.state != CellState.Fixed) {
            val peerValues = getPeerCells(cellData.index, state.cells, solved = true).map(_.cellValue).distinct
            val notes = filledSet diff peerValues
            cellData.copy(state = CellState.Unsolved, possibilities = notes.toSet, cellValue = 0)
          } else {
            cellData
          }
        }.toMap
        setState(state.copy(cells = cells, mode = PuzzleMode.Input,
          lastPossibilities = getLastPossibilities(cells), selectedKey = None))
      case SwitchEvent.Note =>
        if (state.mode == PuzzleMode.Note)
          setState(state.copy(mode = PuzzleMode.Input))
        else
          setState(state.copy(mode = PuzzleMode.Note))
    }
  }

  def onCellClicked(cellIndex: Int): Unit = {
    if (state.selectedKey.isEmpty && cellIndex == 0 && state.cells(cellIndex).possibilities.isEmpty) {
      val puzzle: Seq[Int] = Seq(
        -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1,  2, -1,  7,  8, -1,  1,
        -1, -1,  7, -1,  1,  3, -1, -1, -1,
        -1,  9, -1, -1, -1,  2, -1,  5,  7,
        -1, -1, -1,  7,  4, -1, -1, -1,  6,
        -1,  5, -1,  1, -1, -1,  2, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1,  5,
         1,  8, -1,  4,  2, -1, -1,  6, -1,
        -1, -1, -1, -1,  6, -1,  9, -1, -1
      )

      val cells = (0 until 81).map { i =>
        if (puzzle(i) == -1) {
          val peerValues = peerMap(i).map(puzzle).filter(_ != -1)
          val notes = filledSet diff peerValues

          i -> CellData(i, CellState.Unsolved, notes.toSet, 0, new Animated.Value(0))
        } else {
          i -> CellData(i, CellState.Fixed, Set.empty, puzzle(i), new Animated.Value(0))
        }
      }.toMap
      val lastPossibilities = getLastPossibilities(cells)
      setState(State(cells, lastPossibilities, None, Set.empty, PuzzleMode.Input))
    } else {
      val oldCellData = state.cells(cellIndex)

      // ======================================
      // 1. Process cell click
      // ======================================
      val newCellData = state.selectedKey match {
        case Some(selectedKey) => // If some key is selected
          state.mode match {
            case PuzzleMode.Question =>
              // In question mode, add or remove key to selected cell but only if its valid
              oldCellData.state match {
                case CellState.Unsolved =>
                  val isInvalid =
                    getPeerCells(cellIndex, state.cells, solved = true)
                      .map(_.cellValue).distinct
                      .contains(selectedKey)
                  if (!isInvalid)
                    oldCellData.copy(state = CellState.Fixed, cellValue = selectedKey)
                  else {
                    flashErrorBackground(oldCellData.flashAnim)
                    oldCellData
                  }
                case CellState.Fixed =>
                  if (oldCellData.cellValue == selectedKey)
                    oldCellData.copy(state = CellState.Unsolved, cellValue = 0)
                  else oldCellData
                case _ => oldCellData
              }

            case PuzzleMode.Note =>
              // In noteMode, add or remove note from unsolved cells
              oldCellData.state match {
                case CellState.Unsolved =>
                  val newPossibilities =
                    if (oldCellData.possibilities.contains(selectedKey))
                      oldCellData.possibilities - selectedKey
                    else oldCellData.possibilities + selectedKey
                  oldCellData.copy(possibilities = newPossibilities)
                case _ => oldCellData
              }

            case PuzzleMode.Input =>
              // In keyMode, solve/un-solve non-question cells
              oldCellData.state match {
                case CellState.Solved =>
                  val peerValues = getPeerCells(cellIndex, state.cells, solved = true)
                    .map(_.cellValue).distinct
                  val newPossibilities = filledSet diff peerValues
                  oldCellData.copy(state = CellState.Unsolved, possibilities = newPossibilities.toSet, cellValue = 0)
                case CellState.Unsolved =>
                  oldCellData.copy(state = CellState.Solved, possibilities = Set.empty, cellValue = selectedKey)
                case CellState.Fixed =>
                  oldCellData
              }
          }

        case None if state.mode != PuzzleMode.Question =>
          // If no key is selected and any unsolved cell can be solved, solve it
          if (oldCellData.possibilities.size == 1) {
            oldCellData.copy(state = CellState.Solved, possibilities = Set.empty, cellValue = oldCellData.possibilities.head)
          } else if (state.lastPossibilities.contains(cellIndex)) {
            oldCellData.copy(state = CellState.Solved, possibilities = Set.empty, cellValue = state.lastPossibilities(cellIndex))
          } else oldCellData

        case _ => oldCellData
      }

      // ======================================
      // 2. If added/removed number in keyMode, recalculate possibilities for the peers of that cell
      // ======================================
      val newCells = state.cells.updated(newCellData.index, newCellData)
      val refreshedCells = {
        if (newCellData.state == CellState.Solved) {
          // Remove added value from peer cells
          getPeerCells(newCellData.index, newCells, solved = false)
            .map(peer => peer.copy(possibilities = peer.possibilities - newCellData.cellValue))
            .foldLeft(newCells) { case (cells, cell) => cells.updated(cell.index, cell) }
        } else if (newCellData.state == CellState.Unsolved && oldCellData.state == CellState.Solved) {
          // Add removed value to peer cells if valid
          getPeerCells(newCellData.index, newCells, solved = false)
            .map { peer =>
              // Check if value can be added to this peer
              val shouldAdd = !getPeerCells(peer.index, newCells, solved = true)
                .map(_.cellValue).distinct
                .contains(oldCellData.cellValue)
              if (shouldAdd)
                peer.copy(possibilities = peer.possibilities + oldCellData.cellValue)
              else
                peer.copy(possibilities = peer.possibilities)
            }
            .foldLeft(newCells) { case (cells, cell) => cells.updated(cell.index, cell) }
        } else newCells
      }
      val newLastPossibilities = getLastPossibilities(refreshedCells)

      // ======================================
      // 3. Find all keys to disable
      // ======================================
      val keysToDisable = (1 to 9).filter { i =>
        unitList.forall(unit => unit.count(u => refreshedCells(u).cellValue == i) == 1)
      }.toSet

      val newState =
        if (keysToDisable.nonEmpty) {
          state.copy(cells = refreshedCells, lastPossibilities = newLastPossibilities,
            disabledKeys = keysToDisable, selectedKey = None)
        } else {
          state.copy(cells = refreshedCells, lastPossibilities = newLastPossibilities)
        }

      setState(newState)
    }
  }

  /**
   * Get list of cells that are the last location for a number in a unit
   */
  private def getLastPossibilities(cellData: Map[Int, CellData]): Map[CellIndex, Possibility] = {
    unitList.flatMap { unit =>
      unit.map(cellData)
        .filter(_.state == CellState.Unsolved)
        .flatMap(unitCell => unitCell.possibilities.map(unitCell.index -> _))
        // group cells by possibility
        .groupMap(_._2)(_._1)
        .filter(_._2.size == 1)
        .view.mapValues(_.head).map(_.swap)
        .filterNot { case (index, possibility) =>
          // Exclude possibility which are not valid
          getPeerCells(index, cellData, solved = true)
            .map(_.cellValue).distinct
            .contains(possibility)
        }
        .toMap
    }.toMap
  }

  def render(): ReactElement = {
    View(style = literal(width = "100%"))(
      Grid(state.cells, state.lastPossibilities, state.selectedKey, state.mode, onCellClicked),
      KeyPad(state.selectedKey, state.disabledKeys, state.mode, onKeySelected, switchMode)
    )
  }
}
