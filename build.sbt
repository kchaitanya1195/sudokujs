enablePlugins(ScalaJSPlugin)

name := "SudokuJS"
version := "0.1"
scalaVersion := "2.13.6"

//scalaJSUseMainModuleInitializer := true

libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "1.1.0"

libraryDependencies += "me.shadaj" %%% "slinky-core" % "0.6.7" // core React functionality, no React DOM
libraryDependencies += "me.shadaj" %%% "slinky-web" % "0.6.7" // React DOM, HTML and SVG tags
libraryDependencies += "me.shadaj" %%% "slinky-native" % "0.6.7" // React Native components

// scalacOptions += "-P:scalajs:sjsDefinedByDefault"
scalacOptions += "-Ymacro-annotations"

// scalaJS app can be used as a js module instead of a script to be used in html
scalaJSLinkerConfig ~= (_.withModuleKind(ModuleKind.CommonJSModule))
